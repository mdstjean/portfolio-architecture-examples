= Azure Red Hat OpenShift Implementation
Ricardo Garcia Cavero @rgarciac
:homepage: https://gitlab.com/osspa/portfolio-architecture-examples/
:imagesdir: images
:icons: font
:source-highlighter: prettify
:toc: left
:toclevels: 5

_Some details will differ based on the requirements of a specific implementation but all portfolio architectures generalize one or more successful deployments of a use case._

*Use case:* Adopt cloud native development technologies and mindset using a container platform without having to manage it and controlling its costs and use all the services offered by cloud providers to create more innovative and useful applications.

*Background:* Modernizing the way in which companies develop applications is key for keeping up with the fast pace imposed by the market. They need to be flexible and provide the means for developers to be productive from day one without having to worry about infrastructure details. Deploying a container platform is a great way to achieve this as it caters for the usage of DevOps methodologies, cloud native and micro services technologies without having a mix of many different platforms that are hard to maintain. However some customers do not have the in-house skill set to manage such platforms and might also not want to invest on the large infrastructure that they require and it is for them that a managed container platform like Azure Red Hat OpenShift is a great option.




== Solution overview

The Azure Red Hat OpenShift Implementation architecture tackles the challenge of running many redundant platforms. Instead, all applications can be developed and run on a single platform. 

====
*Azure Red Hat OpenShift implementation*

. Adopt a cloud native solution to migrate applications to and build new ones on, while enabling a hybrid cloud model
. Make the most of all the services offered by Azure when developing applications by integrating them with the container platform
. Reduce the cost of having a permanent dedicated on-premise infrastructure for the container platform as well as the cost and need for skills to manage it

====


--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/intro-marketectures/ms-aro-marketing-slide.png[alt="Deployment of Red Hat OpenShift on MS Azure", width=700]
--
This solution is based on the ARO Landing Zone Accelerator.


== Summary video
video::ZKa2ZoV89uI[youtube]


== Logical diagram
--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/logical-diagrams/ms-aro-ld.png[alt="Red Hat OpenShift on public cloud generic components ", width=700]
--

== The technology

The following technology was chosen for this solution:

====
https://www.redhat.com/en/technologies/cloud-computing/openshift/azure[*MS Azure Cloud*] is the hyperscaler platform on which the implementation of this solution has been based. In this solution, some of the main services of the cloud platform that interact with the OpenShift clusters are highlighted, like the Azure Container Registry and the Azure Key Vault for certificate management.

https://www.redhat.com/en/technologies/cloud-computing/openshift/try-it?intcmp=7013a00000318EWAAY[*Azure Red Hat OpenShift*] is a service on Azure cloud that allows to deploy fully managed OpenShift clusters which provide a Kubernetes container platform. It provides the same functionalities as regular Red Hat OpenShift. The support is provided jointly by MS and Red Hat as well as the maintenance operations to keep it up to date and compliant with both MS and Red Hat's recommendations. In this solution, we follow the best practices included in the Azure Landing Zone Accelerator for ARO to deploy it.
====


== Architectures

=== MS ARO ingress traffic configuration
--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/schematic-diagrams/ms-aro-ingress.png[alt="Ingress traffic configuration for Azure Red Hat OpenShift (ARO)", width=700]
--

This schematic diagram shows the recommended network configuration for the ingress traffic into the ARO cluster together with the main Azure services that will connect to it. 

It also shows how users can access the APIs for the applications running on the ARO cluster through the Azure Front Door service combined with Azure Private Link to create a private endpoint.

Connection to Azure Arc logging and monitoring services is recommended for cluster performance and usage analysis that will trigger recommendations to improve customer use of their clusters.


=== MS ARO egress traffic configuration
--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/schematic-diagrams/ms-aro-egress.png[alt="Egress traffic configuration for Azure Red Hat OpenShift (ARO)", width=700]
--

Here we can see the recommended configuration for the egress traffic from the ARO cluster to the Internet. To filter this traffic the choice for the implementation is to use Azure Firewall service.

For users that need to access the ARO cluster itself, the recommendation is to use Azure Bastion service to create a bastion/jump host to connect to it.

== Download diagrams
View and download all of the diagrams above in our open source tooling site.
--
https://www.redhat.com/architect/portfolio/tool/index.html?#gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/diagrams/ms-aro.drawio[[Open Diagrams]]
--

== Provide feedback
You can offer to help correct or enhance this architecture by filing an https://gitlab.com/osspa/portfolio-architecture-examples/-/blob/main/ms-aro.adoc[issue or submitting a merge request against this Portfolio Architecture product in our GitLab repositories].




