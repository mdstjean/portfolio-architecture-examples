= Edge Medical Diagnosis
Eric D. Schabell @eschabell
:homepage: https://gitlab.com/osspa/portfolio-architecture-examples
:imagesdir: images
:icons: font
:source-highlighter: prettify
:toc: left

_Some details will differ based on the requirements of a specific implementation but all portfolio architectures generalize one or more successful deployments of a use case._

*Use case:* Accelerating medical diagnosis using condition detection in medical imagery with AI/ML in Edge computing at medical facilities.




== Solution overview

This architecture covers the use case around Edge Medical Diagnosis in the healthcare industry. The solution provides a way to increase efficiency of a medical facility by reducing time spent on routine diagnosis and medical diagnosis of disease. This gives the medical expert time to focus on the difficult cases.

====
*Why Edge Medical Diagnosis?*

. Need for faster medical diagnosis of diseases
. Provide expert-assist to medical experts to reduce time spent on routine diagnosis
. Focus medical expert time on most difficult to interpret cases
====


--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/intro-marketectures/edge-medical-diagnosis-marketing-slide.png[750,700]
--

== Logical diagram
--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/logical-diagrams/edge-medical-diagnosis-details-ld.png[750, 700]
--

== The technology

The following technology was chosen for this solution:

====
https://www.redhat.com/en/technologies/cloud-computing/openshift-data-foundation?intcmp=7013a00000318EWAAY[*Red Hat OpenShift Data Foundations*] is a software-defined storage services for X-ray images, continuous deployment models, analytics, AI/ML datasets and models, provisioned across multi-cloud environments.

https://www.redhat.com/en/technologies/cloud-computing/openshift-data-foundation?intcmp=7013a00000318EWAAY[*Red Hat OpenShift Data Foundations*] develops, trains, and tests for AI/ML modeling and visualization in sandbox environment. Diagnosis models are being continuously trained and updated, this streamline workflow allows a more rapid, agile application lifecycle.

https://catalog.redhat.com/software/operators/detail/5ef20efd46bc301a95a1e9a4?intcmp=7013a00000318EWAAY[*Red Hat AMQ Streams*] is a data streaming platform with high throughput and low latency. Streams images and registration events to corresponding microservices to automated diagnosis. Relay diagnosis result for further process, and broadcast notifications.

https://www.redhat.com/en/technologies/cloud-computing/openshift/try-it?intcmp=7013a00000318EWAAY[*Red Hat OpenShift Serverless*] provides event-driven functions and scales up based on an event trigger. Medical application scales down to zero for resource optimization, and starts up with minimal bootstrap time when it is required.

https://www.redhat.com/en/technologies/cloud-computing/openshift/try-it?intcmp=7013a00000318EWAAY[*Red Hat OpenShift GitOps*] automates the deployment of the edge medical diagnosis elements, pick up changes from code repository into the CI/CD pipelines and trigger image build and deploys into clouds.

https://www.redhat.com/en/technologies/cloud-computing/openshift/try-it?intcmp=7013a00000318EWAAY[*Red Hat OpenShift*] Kubernetes container platform with both Serverless and GitOps mentioned above. It provides a consistent application platform to manage supports for full automated workflow and flexible, scalable resource usage.

https://www.redhat.com/en/technologies/linux-platforms/enterprise-linux?intcmp=7013a00000318EWAAY[*Red Hat Enterprise Linux*] is the world’s leading enterprise Linux platform. It’s an open source operating system
(OS). It’s the foundation from which you can scale existing apps—and roll out emerging technologies—across bare-metal,
virtual, container, and all types of cloud environments.
====

== Architectures

=== Edge Medical Diagnosis overview
--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/schematic-diagrams/edge-medical-diagnosis-network-sd.png[750, 700]
--

This is an overview look at Edge Medical Diagnosis, providing the solution details and the elements described above in both a network and data centric view.

The overview splits the solution into two distinct locations; the diagnostic facility where the medical staff and
the edge x-ray devices are located and the medical data center where development and monitoring of the solution takes
place.

Initial images are sent into the diagnostic facility image receiver and register an event to start the processing
for automated diagnosis. These images are stored locally, anonymized, and automatically evaluated for possible
disease detection. A notification is generated for the medical staff, either automated detection, non-detection, or
an edge case needing qualified medical staff review.

In the process of image capture and processing, the images are sent back to the medical data center to be added
to the collection used for model training and development. The applications, machine learning models, data science
development and dashboards for monitoring the processes are all in constant evolution. Developers and operations
teams are maintaining code and infrastructure manifests for full GitOps deployment of the architectural elements.

=== Edge Medical Diagnosis with GitOps
--
image:https://gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/images/schematic-diagrams/edge-medical-diagnosis-gitops-sd.png[750, 700]

--

GitOps delivery and development are essential to a fully automated cloud hosted solution. This schematic diagram features the elements focusing only on development and deployment of the Edge Medical Diagnosis elements needed for this solution. It removes the patient facing medical staff and the edge image capturing, instead featuring developer and IT operations staff on the back end.

In the medical data center, developers deliver code projects into the CI/CD pipelines and trigger eventual container image builds put into the registry. The same is happening on the IT operations side, where system configuration and
manifest code is maintained in their repository.

The developer image registry is replicated out to the image registry in the remote diagnostic facility and the source code repository for IT operations is also replicated out to the remote location. These both are setup to
trigger the GitOps pipelines to sync updates to the image registry and the operation's source code repository to the OpenShift platform. This means it's deploying, configuring, and applying manifests to the applications and services
used to process the medical diagnosis imaging solution.

== Download diagrams
View and download all of the diagrams above in our open source tooling site.
--
https://www.redhat.com/architect/portfolio/tool/index.html?#gitlab.com/osspa/portfolio-architecture-examples/-/raw/main/diagrams/edge-medical-diagnosis.drawio[[Open Diagrams]]
--

== Provide feedback on this architecture
You can offer to help correct or enhance this architecture by filing an https://gitlab.com/osspa/portfolio-architecture-examples/-/blob/main/edge-medical-diagnosis.adoc[issue or submitting a merge request against this Portfolio Architecture product in our GitLab repositories].
